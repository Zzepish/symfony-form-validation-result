<?php
/**
 * Short description for class
 *
 * Long description for class (if any)...
 *
 * @category   CategoryName
 * @package    PackageName
 * @author     Original Author <author@example.com>
 * @author     Another Author <another@example.com>
 * @copyright  1997-2005 The PHP Group
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      Class available since Release 1.2.0
 * @deprecated Class deprecated in Release 2.0.0
 */

namespace Zzepish\SymfonyFormUtils;

use Symfony\Component\Form\FormInterface;

class FormValidationInfo
{
    const RESPONSE_STATUS_SUCCESS      = 'SUCCESS';
    const RESPONSE_STATUS_NOTIFICATION = 'NOTIFICATION';
    const RESPONSE_STATUS_FAIL         = 'FAIL';

    private FormInterface $form;
    private bool $is_valid = true;
    private FormValidationResult $formValidationResult;

    public function validateForm(string $success_message): FormValidationResult
    {
        $this->is_valid   = true;
        $this->formValidationResult = new FormValidationResult();
        $this->formValidationResult->setStatus(self::RESPONSE_STATUS_NOTIFICATION);
        $this->formValidationResult->addMessage(new FormValidationMessage('Form was not submitted!', ''));

        if (!$this->form) {
            throw new \Exception('No form was set!');
        }

        if ($this->is_valid = $this->form->isSubmitted()) {
            $this->formValidationResult->clearMessages();

            if (!$this->form->isValid()) {
                $this->is_valid     = false;
                $this->formValidationResult->setStatus(self::RESPONSE_STATUS_FAIL);
                foreach ($this->form->getErrors(true) as $error) {
                    $elementId = $error->getOrigin()->createView()->vars['id'];
                    $this->formValidationResult->addMessage(new FormValidationMessage($error->getMessage(), $elementId));
                }
            } else {
                $this->formValidationResult->setStatus(self::RESPONSE_STATUS_SUCCESS);
                $this->formValidationResult->addMessage(new FormValidationMessage($success_message));
            }
        }

        return $this->formValidationResult;
    }

    public function getForm(): ?FormInterface
    {
        return $this->form;
    }

    public function setForm(FormInterface $form): void
    {
        $this->form = $form;
    }

    public function isValid(): bool
    {
        return $this->is_valid;
    }
}