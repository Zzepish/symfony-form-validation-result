<?php

namespace Zzepish\SymfonyFormUtils;

class FormValidationResult
{
    public const NOTIFICATION_ELEMENT_ID = 'notification';

    protected string $status;
    /**
     * @var FormValidationMessage[][] $messages
     */
    protected array $messages = [];

    public function addMessage(FormValidationMessage $message): void
    {
        $elementId = $message->getElementId();

        if($elementId == '') {
            $elementId = self::NOTIFICATION_ELEMENT_ID;
        }

        if (!array_key_exists($elementId, $this->messages)) {
            $this->messages[$elementId] = [];
        }

        $this->messages[$elementId][] = $message;
    }

    public function clearMessages(): void
    {
        $this->messages = [];
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function __toArray(): array
    {
        $result = ['status' => $this->status, 'messages' => []];
        foreach ($this->messages as $element => $messages) {
            $result['messages'][$element] = [];
            foreach ($messages as $message) {
                $result['messages'][$element][] = $message->__toArray();
            }
        }

        return $result;
    }
}