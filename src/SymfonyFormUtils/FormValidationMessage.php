<?php

namespace Zzepish\SymfonyFormUtils;

class FormValidationMessage
{
    protected string $message;
    protected string $elementId;

    public function __construct(string $message, string $elementId = '')
    {
        $this->message   = $message;
        $this->elementId = $elementId;
    }

    public function __toArray(): array
    {
        return [
            'message'    => $this->message,
            'element_id' => $this->elementId,
        ];
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getElementId(): string
    {
        return $this->elementId;
    }


}